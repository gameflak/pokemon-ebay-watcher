﻿//using EbayWatcher;

//WatcherFunctionality WF = new WatcherFunctionality();
//WF.Init();
//await WF.Run();

using Discord;
using Discord.Interactions;
using Discord.Net;
using Discord.WebSocket;
using EbayWatcher;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Reflection;
using System.Windows.Input;

public class Program {
    private DiscordSocketClient _client;
    private InteractionService _commands;

    private Program() {}

    private async Task _client_Ready() {
        await _commands.RegisterCommandsGloballyAsync(true);
    }

    public static Task Main(string[] args) => new Program().MainAsync();
    public async Task MainAsync() {
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        IConfiguration configuration = configurationBuilder.AddUserSecrets<Program>().Build();

        IServiceProvider services = ConfigureServices();
        var client = services.GetRequiredService<DiscordSocketClient>();
        var commands = services.GetRequiredService<InteractionService>();

        _client = client;
        _commands = commands;

        client.Log += Log;
        commands.Log += Log;
        client.Ready += _client_Ready;


        await _client.LoginAsync(TokenType.Bot, configuration["botToken"]);
        await _client.StartAsync();

        await services.GetRequiredService<CommandHandler>().InitializeAsync();

        await Task.Delay(-1);
    }

    public static IServiceProvider ConfigureServices() {
        IServiceCollection services = new ServiceCollection();
        services.AddSingleton<DiscordSocketClient>();
        services.AddSingleton<WatcherFunctionality>();
        services.AddSingleton(x => new InteractionService(x.GetRequiredService<DiscordSocketClient>()));
        services.AddSingleton<CommandHandler>();

        return services.BuildServiceProvider();
    }

    private static Task Log(LogMessage message) {
        switch (message.Severity) {
            case LogSeverity.Critical:
            case LogSeverity.Error:
                Console.ForegroundColor = ConsoleColor.Red;
                break;
            case LogSeverity.Warning:
                Console.ForegroundColor = ConsoleColor.Yellow;
                break;
            case LogSeverity.Info:
                Console.ForegroundColor = ConsoleColor.White;
                break;
            case LogSeverity.Verbose:
            case LogSeverity.Debug:
                Console.ForegroundColor = ConsoleColor.DarkGray;
                break;
        }
        Console.WriteLine($"{DateTime.Now,-19} [{message.Severity,8}] {message.Source}: {message.Message} {message.Exception}");
        Console.ResetColor();

        return Task.CompletedTask;
    }
}