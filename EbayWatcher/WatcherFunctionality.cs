﻿using HtmlAgilityPack;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.LinearAlgebra.Factorization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using static System.Net.Mime.MediaTypeNames;

namespace EbayWatcher {
    public class WatcherFunctionality {
        public static List<string> pokemon;
        public static List<CardInfo> infos = new List<CardInfo>();
        public static List<SetInfo> sets = new List<SetInfo>();

        public WatcherFunctionality() {
            if (pokemon == null) {
                pokemon = File.ReadAllText("assets/pokemon-list-en.txt").Split("\n").ToList();
            }
            if (sets.Count == 0) {
                sets.Add(new SetInfo() {
                    Name = "Ruby and Sapphire",
                    Names = new List<string> { "ruby", "sapphire" },
                    PriceChartingName = "pokemon-ruby-&-sapphire",
                    FileName = "ex1"
                });
                sets.Add(new SetInfo() {
                    Name = "Sandstorm",
                    Names = new List<string> { "sandstorm" },
                    PriceChartingName = "pokemon-sandstorm",
                    FileName = "ex2"
                });
                sets.Add(new SetInfo() {
                    Name = "Team Magma vs Team Aqua",
                    Names = new List<string> { "team magma", "team aqua" },
                    PriceChartingName = "pokemon-team-magma-&-team-aqua",
                    FileName = "ex4"
                });
                sets.Add(new SetInfo() {
                    Name = "Hidden Legends",
                    Names = new List<string> { "hidden legend" },
                    PriceChartingName = "pokemon-hidden-legends",
                    FileName = "ex5"
                });
                sets.Add(new SetInfo() {
                    Name = "Firered & Leafgreen",
                    Names = new List<string> { "firered", "leafgreen" },
                    PriceChartingName = "pokemon-firered-&-leafgreen",
                    FileName = "ex6"
                });
                sets.Add(new SetInfo() {
                    Name = "Team Rocket Returns",
                    Names = new List<string> { "team rocket returns" },
                    PriceChartingName = "pokemon-team-rocket-returns",
                    FileName = "ex7"
                });
                sets.Add(new SetInfo() {
                    Name = "EX Deoxys",
                    Names = new List<string> { "deoxys ex", "ex deoxys" },
                    PriceChartingName = "pokemon-deoxys",
                    FileName = "ex8"
                });
                sets.Add(new SetInfo() {
                    Name = "EX Emerald",
                    Names = new List<string> { "emerald ex", "ex emerald" },
                    PriceChartingName = "pokemon-emerald",
                    FileName = "ex9"
                });
                sets.Add(new SetInfo() {
                    Name = "Unseen Forces",
                    Names = new List<string> { "unseen forces" },
                    PriceChartingName = "pokemon-unseen-forces",
                    FileName = "ex10"
                });
                sets.Add(new SetInfo() {
                    Name = "EX Legend Maker",
                    Names = new List<string> { "legend maker" },
                    PriceChartingName = "pokemon-legend-maker",
                    FileName = "ex12"
                });
                sets.Add(new SetInfo() {
                    Name = "EX Holon Phantoms",
                    Names = new List<string> { "holon phantom" },
                    PriceChartingName = "pokemon-holon-phantoms",
                    FileName = "ex13"
                });
                sets.Add(new SetInfo() {
                    Name = "EX Crystal Guardians",
                    Names = new List<string> { "crystal guardian" },
                    PriceChartingName = "pokemon-crystal-guardians",
                    FileName = "ex14"
                });
                sets.Add(new SetInfo() {
                    Name = "EX Dragon Frontiers",
                    Names = new List<string> { "dragon frontier" },
                    PriceChartingName = "pokemon-dragon-frontiers",
                    FileName = "ex15"
                });
                sets.Add(new SetInfo() {
                    Name = "Dragon",
                    Names = new List<string> { "dragon" },
                    PriceChartingName = "pokemon-dragon",
                    FileName = "ex3"
                });
                sets.Add(new SetInfo() {
                    Name = "EX Power Keepers",
                    Names = new List<string> { "power keeper" },
                    PriceChartingName = "pokemon-power-keepers",
                    FileName = "ex16"
                });
                sets.Add(new SetInfo() {
                    Name = "EX Delta Species",
                    Names = new List<string> { "delta species" },
                    PriceChartingName = "pokemon-delta-species",
                    FileName = "ex11"
                });
            }
            if (infos.Count == 0) {
                foreach (var set in sets) {
                    List<CardInfo> setList = JsonConvert.DeserializeObject<List<CardInfo>>(File.ReadAllText($"assets/setlist/{set.FileName}.json"));
                    setList.ForEach(x => x.set = set);
                    infos = infos.Concat(setList).ToList();
                }
            }
        }

        public async Task Run() {
            List<EbayInfo> es = new List<EbayInfo>();

            es = await LoadListings();

            List<LookupResult> results = new List<LookupResult>();
            foreach (var e in es) {
                if (!e.Card.isReverse) continue;
                if (e.TimeLeft > 1440) continue;
                string url = $"https://www.pricecharting.com/game/{e.Card.set.PriceChartingName}/{e.Card.name.Replace("★", "gold-star").Replace(" δ", "").Replace(" ♂", "").Replace(" ", "-")}{(e.Card.isReverse ? "-reverse-holo" : "")}-{e.Card.number}";
                LookupResult lr = await GetPriceChart(url, e.Card.Grade);

                lr.logs.Prepend(url);
                lr.logs.Prepend($"https://www.ebay.com/itm/{e.ID}");
                lr.logs.Prepend(String.Format("Ebay price: {0}, shipping: {1}", e.Price, e.Shipping));
                lr.logs.Prepend("End time: " + DateTime.Now.AddMinutes(e.TimeLeft).ToString());

                if (lr.NoSales) {

                } else if (lr.LowSales) {
                    lr.logs.Add(String.Format("Low Sales: {1}, Last sale: ${0}", lr.LastSale, lr.SalesCount));
                    if ((double)e.TotalPrice < (double)lr.LastSale * .9) {
                        lr.type = ResultType.Good;
                    } else {
                        lr.type = ResultType.Bad;
                    }
                } else {
                    if ((double)e.TotalPrice < lr.Prediction * .9) {
                        lr.type = ResultType.Good;
                        lr.logs.Add("Target bid max: $" + Math.Round(lr.Prediction * .8, 2));
                    } else if ((double)e.TotalPrice > lr.Prediction * .9 && (double)e.TotalPrice < lr.Prediction * 1.05) {
                        lr.type = ResultType.Mid;
                    } else {
                        lr.type = ResultType.Bad;
                    }
                }
                results.Add(lr);
            }

            foreach (LookupResult lr in results) {
                if (lr.type != ResultType.Good) continue;
                Console.WriteLine(lr.type.ToString() + " Deal");
                foreach (string s in lr.logs) {
                    Console.WriteLine(s);
                }
                Console.WriteLine();
            }
        }

        public async Task<LookupResult> GetPriceChart(string url, string grade) {
            LookupResult lr = new LookupResult();
            using (HttpClient client = new HttpClient()) {
                lr.logs.Add(url);
                string resp = await client.GetStringAsync(url);
                var doc = new HtmlDocument();
                doc.LoadHtml(resp);
                var Grade9 = doc.DocumentNode.SelectNodes("//div[@class='completed-auctions-graded']/table/tbody/tr");
                var Grade10 = doc.DocumentNode.SelectNodes("//div[@class='completed-auctions-manual-only']/table/tbody/tr");

                HtmlNodeCollection? useTable = null;
                if (grade != null && grade.StartsWith("10")) {
                    useTable = Grade10;
                } else {
                    useTable = Grade9;
                }
                if (grade != null) lr.logs.Add("Grade: " + grade);

                List<SalesPoints> sales = new List<SalesPoints>();
                if (useTable == null) {
                    lr.type = ResultType.NA;
                    lr.logs.Add("No Sales recorded.");
                    lr.NoSales = true;
                } else {
                    foreach (var row in useTable.Reverse()) {
                        string date = row.SelectNodes("td[@class='date']").First().InnerText;
                        string title = row.SelectNodes("td[@class='title']/a").First().InnerText;
                        string price = row.SelectNodes("td[@class='numeric']/span").First().InnerText;
                        price = Regex.Replace(price, "[^\\d\\.]", "");
                        sales.Add(new SalesPoints() {
                            SalesDate = DateTime.Parse(date),
                            Price = Decimal.Parse(price)
                        });
                    }
                    lr.SalesCount = sales.Count;
                    lr.LastSale = (double)sales.Last().Price;
                    if (sales.Count > 4) {
                        var x = sales.Select(x => (double)x.SalesDate.ToOADate()).ToArray();
                        var y = sales.Select(x => (double)x.Price).ToArray();
                        Dictionary<int, double[]> polys = new Dictionary<int, double[]>();
                        var ord = 1;
                        while (ord < 6) {
                            try {
                                double[] p1 = Polyfit(x, y, ord);

                                polys.Add(ord, p1);

                                ord++;
                            } catch {
                                break;
                            }
                        }

                        var polyOrder = polys.Select(poly =>
                        {
                            return new {
                                degree = poly.Key,
                                p = poly.Value,
                                diff = Math.Abs(x.Select((x2, i2) => Math.Abs(y[i2] - Polynomial.Evaluate(x[i2], poly.Value))).Average())
                            };
                        }).OrderBy(poly => poly.diff);

                        var p = polyOrder.First(x => x.degree == Math.Max(1d, polyOrder.First().degree - 1));

                        for (int i = 0; i < x.Length; i++)
                            lr.logs.Add(String.Format("{0} => Expected: ${1}, Actual: ${2}, Diff: ${3}", DateTime.FromOADate(x[i]).ToString("MM/dd/yyyy"), Math.Round(Polynomial.Evaluate(x[i], p.p), 2), Math.Round(y[i], 2), Math.Round(y[i] - Polynomial.Evaluate(x[i], p.p), 2)));


                        double nextX = x.Last() + 10;
                        double nextY = Polynomial.Evaluate(nextX, p.p);
                        lr.Prediction = Math.Round(nextY, 2);
                        lr.logs.Add(String.Format("Prediction => ${0}", Math.Round(nextY, 2)));
                    } else {
                        lr.LowSales = true;
                    }
                }
            }
            return lr;
        }

        private double[] Polyfit(double[] x, double[] y, int degree) {
            // Vandermonde matrix
            var v = new DenseMatrix(x.Length, degree + 1);
            for (int i = 0; i < v.RowCount; i++)
                for (int j = 0; j <= degree; j++) v[i, j] = Math.Pow(x[i], j);
            var yv = new DenseVector(y).ToColumnMatrix();
            QR<double> qr = v.QR();
            // Math.Net doesn't have an "economy" QR, so:
            // cut R short to square upper triangle, then recompute Q
            var r = qr.R.SubMatrix(0, degree + 1, 0, degree + 1);
            var q = v.Multiply(r.Inverse());
            var p = r.Inverse().Multiply(q.TransposeThisAndMultiply(yv));
            return p.Column(0).ToArray();
        }

        private async Task<EbayInfo> getEbayInfo(string url, HtmlNode node) {
            var ebay = new EbayInfo();
            if (url.Contains("/itm/")) {
                string price = node.DescendantNodes().FirstOrDefault(x => x.Attributes["class"]?.Value == "x-price-primary")?.InnerText;
                if (price == null) return new EbayInfo {
                    Found = false
                };
                price = Regex.Replace(price, "[^\\d\\.]", "");
                string shipping = node.DescendantNodes().FirstOrDefault(x => x.Attributes["class"]?.Value == "ux-labels-values col-12 ux-labels-values--shipping")?
                    .DescendantNodes().FirstOrDefault(x => x.Attributes["class"]?.Value == "ux-textspans ux-textspans--BOLD")?.InnerText;

                shipping = Regex.Replace(shipping ?? "", "[^\\d\\.]", "");
                if (shipping == "") shipping = "0.00";

                string name = node.DescendantNodes().FirstOrDefault(x => x.Attributes["class"]?.Value == "x-item-title__mainTitle")?.InnerText;
                var cardInfo = figureOutCard(name);

                string id = Regex.Match(url, "\\/itm\\/(\\d+)?").Groups[1].Value;
                //Console.WriteLine(name);
                string timeLeft = node.DescendantNodes().FirstOrDefault(x => x.Attributes["class"]?.Value == "ux-timer__text")?.InnerText ?? "0";
                int minutes = 0;
                Dictionary<string, int> timeCalc = new Dictionary<string, int>() {
                    {"m", 1 },
                    {"h", 60},
                    {"d", 60 * 24},
                };
                foreach (var time in timeCalc) {
                    var m = Regex.Match(timeLeft, $"(\\d+){time.Key}");
                    if (m.Success) {
                        minutes += Int32.Parse(m.Groups[1].Value) * time.Value;
                    }
                }

                if (cardInfo != null) {
                    ebay = new EbayInfo {
                        ID = id,
                        Price = Decimal.Parse(price),
                        Shipping = decimal.Parse(shipping),
                        Card = cardInfo,
                        Name = name,
                        TimeLeft = minutes
                    };
                }
            } else {
                string price = node.DescendantNodes().FirstOrDefault(x => x.Attributes["class"]?.Value == "s-item__price")?.InnerText;
                if (price == null) return new EbayInfo {
                    Found = false
                };
                price = Regex.Replace(price, "[^\\d\\.]", "");
                string shipping = node.DescendantNodes().FirstOrDefault(x => x.Attributes["class"]?.Value == "s-item__shipping s-item__logisticsCost")?.InnerText;

                shipping = Regex.Replace(shipping ?? "", "[^\\d\\.]", "");
                if (shipping == "") shipping = "0.00";

                string name = node.DescendantNodes().FirstOrDefault(x => x.Attributes["role"]?.Value == "heading")?.InnerText;
                var cardInfo = figureOutCard(name);

                string link = node.DescendantNodes().FirstOrDefault(x => x.Attributes["href"]?.Value.Contains("/itm/") ?? false)?.Attributes["href"].Value;
                string id = Regex.Match(link, "\\/itm\\/(\\d+)?").Groups[1].Value;
                //Console.WriteLine(name);
                string timeLeft = node.DescendantNodes().FirstOrDefault(x => x.Attributes["class"]?.Value == "s-item__time-left")?.InnerText ?? "0";
                int minutes = 0;
                Dictionary<string, int> timeCalc = new Dictionary<string, int>()
                {
                {"m", 1 },
                {"h", 60},
                {"d", 60 * 24},
            };
                foreach (var time in timeCalc) {
                    var m = Regex.Match(timeLeft, $"(\\d+){time.Key}");
                    if (m.Success) {
                        minutes += Int32.Parse(m.Groups[1].Value) * time.Value;
                    }
                }

                if (cardInfo != null) {
                    ebay = new EbayInfo {
                        ID = id,
                        Price = Decimal.Parse(price),
                        Shipping = decimal.Parse(shipping),
                        Card = cardInfo,
                        Name = name,
                        TimeLeft = minutes
                    };
                }
            }
            return ebay;
        }

        public async Task<LookupResult> GetByEbayListing(string url) {
            using (HttpClient client = new HttpClient()) {
                string resp = await client.GetStringAsync(url);
                var doc = new HtmlDocument();
                doc.LoadHtml(resp);
                var e = await getEbayInfo(url, doc.DocumentNode);

                string pc_url = $"https://www.pricecharting.com/game/{e.Card.set.PriceChartingName}/{e.Card.name.Replace("★", "gold-star").Replace(" δ", "").Replace(" ♂", "").Replace(" ", "-")}{(e.Card.isReverse ? "-reverse-holo" : "")}-{e.Card.number}";
                LookupResult lr = await GetPriceChart(pc_url, e.Card.Grade);


                lr.logs.Insert(0, "End time: " + DateTime.Now.AddMinutes(e.TimeLeft).ToString());
                lr.logs.Insert(0, String.Format("Current Ebay price: {0}, shipping: {1}", e.Price, e.Shipping));
                lr.logs.Insert(0, $"https://www.ebay.com/itm/{e.ID}");

                if (lr.NoSales) {

                } else if (lr.LowSales) {
                    lr.logs.Add(String.Format("Low Sales: {1}, Last sale: ${0}", lr.LastSale, lr.SalesCount));
                    if ((double)e.TotalPrice < (double)lr.LastSale * .9) {
                        lr.type = ResultType.Good;
                    } else {
                        lr.type = ResultType.Bad;
                    }
                } else {
                    if ((double)e.TotalPrice < lr.Prediction * .9) {
                        lr.type = ResultType.Good;
                        lr.logs.Add("Good deal target bid max: $" + Math.Round(lr.Prediction * .9, 2));
                    } else if ((double)e.TotalPrice > lr.Prediction * .9 && (double)e.TotalPrice < lr.Prediction * 1.05) {
                        lr.type = ResultType.Mid;
                    } else {
                        lr.type = ResultType.Bad;
                    }
                }
                //lr.logs.Add(lr.type.ToString() + " Deal");
                return lr;
            }
        }

        private async Task<List<EbayInfo>> LoadListings() {
            List<EbayInfo> ebay = new List<EbayInfo>();

            using (HttpClient client = new HttpClient()) {
                string url = "https://www.ebay.com/sch/i.html?_dcat=183454&_fsrp=1&LH_Auction=1&rt=nc&Set=EX%2520Deoxys%7CEX%2520Holon%2520Phantoms%7CEX%2520Team%2520Rocket%2520Returns%7CEX%2520Emerald%7CEX%2520Unseen%2520Forces%7CEX%2520Delta%2520Species%7CEX%2520Legend%2520Maker%7CEX%2520Crystal%2520Guardians%7CEX%2520Dragon%2520Frontiers%7CEX%2520Power%2520Keepers&_svsrch=1&Grade=9%7C10%7C9%252E5&Graded=Yes&_nkw=pokemons&_sop=1";
                string resp = await client.GetStringAsync(url);
                var doc = new HtmlDocument();
                doc.LoadHtml(resp);
                var nodes = doc.DocumentNode.SelectNodes("//li").Where(x => x.Attributes.Contains("data-view"));
                foreach (var node in nodes) {
                    ebay.Add(await getEbayInfo(url, node));
                }
            }
            return ebay;
        }

        private CardInfo? figureOutCard(string text) {
            text = text.ToLower();

            var set = sets.FirstOrDefault(x => x.Names.Exists(y => text.Contains(y)));
            if (set != null) {
                foreach (var item in set.Names) {
                    text = text.Replace(item, "");
                }
            }
            string poke = pokemon.FirstOrDefault(x => text.Contains(x));
            List<Regex> numMatches = new List<Regex>();
            numMatches.Add(new Regex("#(\\d{1,3})"));
            numMatches.Add(new Regex("(\\d{1,3})/"));
            string num = "";
            foreach (var r in numMatches) {
                var m = r.Match(text);
                if (m.Success) {
                    num = r.Match(text).Groups[1].ToString();
                    break;
                }
            }
            var info = infos.FirstOrDefault(x =>
                (poke == null || x.name.ToLower().Contains(poke)) &&
                (x.number == num || String.IsNullOrEmpty(num)) &&
                (set == null || x.set.FileName == set.FileName)
            );
            if (info == null) {
                return null;
            }
            if (text.Contains("reverse")) {
                info.isReverse = true;
            }
            List<string> graders = new List<string> { "cgc", "psa", "beckett" };
            foreach (var grade in graders) {
                var m = Regex.Match(text, grade + " ([\\d\\.]{1,3})");
                if (m.Success) {
                    info.Grade = m.Groups[1].ToString();
                    break;
                }
            }

            return info;
        }
    }

    public class CardInfo {
        public SetInfo set { get; set; }
        public string name { get; set; }
        public string number { get; set; }
        public bool isReverse { get; set; }
        public string Grade { get; set; }
    }

    public class SetInfo {
        public string Name { get; set; }
        public List<string> Names { get; set; }
        public string FileName { get; set; }
        public string PriceChartingName { get; set; }
    }

    public class EbayInfo {
        public string ID { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal Shipping { get; set; }
        public decimal TotalPrice {
            get {
                return this.Price + this.Shipping;
            }
        }
        public CardInfo Card { get; set; }
        public int TimeLeft { get; set; }

        public bool Found { get; set; }

    }
    public class SalesPoints {
        public DateTime SalesDate { get; set; }
        public decimal Price { get; set; }
    }

    public class LookupResult {
        public ResultType type { get; set; }
        public List<string> logs { get; set; } = new List<string>();
        public double Prediction { get; set; }

        public int SalesCount { get; set; }
        public double LastSale { get; set; }
        public bool LowSales { get; set; }
        public bool NoSales { get; set; }
    }

    public enum ResultType {
        Good,
        Mid,
        Bad,
        NA
    }
}
