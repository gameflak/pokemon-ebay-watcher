﻿using Discord;
using Discord.Interactions;
using System.Diagnostics;

namespace EbayWatcher.Commands {
    public class LookupCommands : InteractionModuleBase<SocketInteractionContext> {
        private CommandHandler _handler;
        private readonly WatcherFunctionality _functionality;

        public LookupCommands(CommandHandler handler, WatcherFunctionality watcherFunctionality) {
            _handler = handler;
            _functionality = watcherFunctionality;
        }

        [SlashCommand("getslabsales", "Gets a list of slab sales by grade from price charting.")]
        
        public async Task GetSlabSalesAsync([Summary("PriceChartUrl", "PriceCharting url")] string url, [Summary("Grade", "Card grade. 9 and 10 supported")] string grade) { 
            var results = await _functionality.GetPriceChart(url, grade);
            EmbedBuilder builder = new EmbedBuilder();
            builder.WithDescription(String.Join(Environment.NewLine, results.logs));
            builder.Color = Color.Green;
            Console.WriteLine("getslabsales: {0}, {1}", url, grade);
            await RespondAsync(embed: builder.Build());
        }

        [SlashCommand("getebayinfo", "[TESTING] Currently only works for EX era main sets.")]
        public async Task GetEbayPriceChartAsync(
            [Summary("ebayurl", "the url of the listing to look up")] string url
        ) {
            var result = await _functionality.GetByEbayListing(url);
            EmbedBuilder builder = new EmbedBuilder();
            builder.WithDescription(String.Join(Environment.NewLine, result.logs));
            builder.Color = Color.Green;
            Console.WriteLine("getebayinfo: {0}", url);
            await RespondAsync(embed: builder.Build());
        }
    }
}
